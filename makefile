#------------------------------------------------------------------------------
#
# FAK	 APPLICATION - MAKEFILE
# By FIST Kresimir Mandic 29.03.1999.
#
#------------------------------------------------------------------------------
#TD           = T:\fak\ 
TD           = X:\fak\ 
YTD          = Y:\NewVer.RAR\fak\ 
AD           = Y:\fak\ 
COMD        = .\lib\fakcommon.fle 
#------------------------------------------------------------------------------
# Compiler options
#------------------------------------------------------------------------------
## CC          = start /w c:\snova\sncd\nova
CC          = start /w c:\vscd\bin\nova
CFLAGS      = -w -ereadfgl
#------------------------------------------------------------------------------

FAKFILES   = $(TD)fak_dd.fle $(TD)fak_flds.fle $(TD)fak.fle $(TD)fakpass.fle \
	$(TD)fak_art.fle  $(TD)fakbroj.fle  $(TD)faklock.fle   $(TD)fak_zag.fle  \
	$(TD)fak_stav.fle $(TD)fak_pri.fle  $(TD)fak_pris.fle  $(TD)fak_a_dd.fle \
	$(TD)fakpren.fle  $(TD)fakpren2.fle $(TD)lfaktstd.fle  $(TD)faktext.fle  \
	$(TD)fak_star.fle $(TD)lfak_art.fle $(TD)fak_prm.fle   $(TD)fak_ppri.fle \
	$(TD)fak_ura.fle  $(TD)lfak_pre.fle $(TD)pselfakt.fle  $(TD)lfaktna1.fle \
	$(TD)lfaktda1.fle $(TD)lfaktnb1.fle	$(TD)lfaktdb1.fle  $(TD)fak_ngod.fle \
	$(TD)lfaktext.fle $(TD)fak_ppar.fle $(TD)fakdbutl.fle  $(TD)lfaktdc1.fle \
	$(TD)lfaktnc1.fle $(TD)fak_lang.fle $(TD)fak_ctar.fle  $(TD)fak_start.fle  \
	$(TD)lfaktnj1.fle $(TD)fakprirn.fle $(TD)lfaktnd1.fle  $(TD)fak_default.fle	\
	$(TD)lfaktdd1.fle $(TD)lfaktnj2.fle $(TD)lfakmpr1.fle  $(TD)Afaktnb1.fle \
	$(TD)lfaktdj2.fle $(TD)selfakpr.fle $(TD)lfaktnr1.fle  $(TD)lfaktdr1.fle \
    $(TD)fak_idx.fle  $(TD)faktpbr.fle  $(TD)l_fakbroj.fle $(TD)fak_mpupl.fle \
    $(TD)l_faktpbr.fle $(TD)lfaktrba.fle $(TD)fakgrart.fle $(TD)fakartgr.fle \
    $(TD)fakgr_rekap.fle $(TD)lfakgrart.fle $(TD)lfakartgr.fle $(TD)fak_blag.fle \
    $(TD)fak_nazfak.fle $(TD)aruspro.fle $(TD)fblag.fle $(TD)grp_arzm.fle  \
    $(TD)ref_arzm.fle $(TD)lfaktng1.fle $(TD)lnazfak_err.fle $(TD)lnazfak_ppar.fle \
    $(TD)lista_arzm.fle $(TD)lgrupe_arzm.fle $(TD)lref_arzm.fle $(TD)lkup_usl.fle \
    $(TD)lkup_arzm.fle $(TD)arzm_blag.fle $(TD)arzm_allup.fle $(TD)arzm_avans.fle \
    $(TD)lfaktng2.fle $(TD)uplatnica.fle $(TD)lfaktnj3.fle $(TD)lfaktnj4.fle \
    $(TD)lfakt_nazfak.fle $(TD)lfak_strval.fle $(TD)lfaktdr1_ml.fle $(TD)arzm_file.fle \
    $(TD)lclose_kalk.fle $(TD)usluge_file.fle $(TD)larzm_sk.fle $(TD)fak_rabati.fle \
    $(TD)lfaktnb1_rab.fle $(TD)lpok_arzm.fle $(TD)lfaktdr1_mls.fle $(TD)karticagt.fle \
    $(TD)fakgr_acrek.fle $(TD)lkup_ref.fle $(TD)lfaktng3.fle $(TD)lfaktnj5.fle \
    $(TD)fakparjez.fle $(TD)garancije.fle $(TD)odobreno.fle $(TD)lgarancije.fle \
    $(TD)lfaktnh1.fle $(TD)lfakt_isp.fle $(TD)lponuda.fle $(TD)txt_pdv0.fle   \
    $(TD)logo_nb1.fle $(TD)lfaktnj6.fle $(TD)fak_str_sld.fle $(TD)loadinka.fle \
    $(TD)faktip.fle $(TD)storno1za1.fle $(TD)fakgr_pprek.fle $(TD)lfaktne1.fle \
    $(TD)lfaktne2.fle $(TD)lfaktns1.fle $(TD)loadinkast.fle $(TD)lfaktnj7.fle \
    $(TD)fak_rev.fle $(TD)fakgt_pprek.fle $(TD)lfaktng5.fle $(TD)lfaktnj8.fle \
    $(TD)logo_nb2.fle $(TD)lfaktnr6.fle $(TD)lfakt_lsf.fle $(TD)fakture_ext.fle \
    $(TD)fak_ext_st.fle 

FAKFILES2   = $(TD)fakturisti.fle $(TD)fak_2012.fle $(TD)stav_2012.fle $(TD)lfakthub.fle \
    $(TD)lfaktna2.fle $(TD)lfaktnj9.fle $(TD)lfaktno1.fle $(TD)lfaktnc2.fle \
    $(TD)lfaktne3.fle $(TD)lponuda2.fle $(TD)lponuda3.fle $(TD)lfaktnc3.fle $(TD)lfaktda2.fle \
    $(TD)storno_dio.fle $(TD)fak_zag_gt.fle $(TD)lfaktdj9.fle $(TD)lfaktnm1.fle \
    $(TD)lfaktnc4.fle $(TD)fak_hpagot.fle $(TD)hpa_zag.fle $(TD)lfaktdt1.fle \
    $(TD)hpa_stavke.fle $(TD)lfaktnt1.fle $(TD)fak_pokrica.fle $(TD)load_milk.fle \
    $(TD)lfaktnh2.fle $(TD)lfakt_eaap.fle 

COMMON     = aruspro.ls arzm_blag.ls fak_a_dd.ls fak_art.ls fak_dd.ls fak_ddx.ls \
            fak_default.ls fak_flds.ls fak_idx.ls fak_lang.ls fak_ngod.ls fak_ppar.ls \
            fak_ppri.ls fak_pri.ls fak_prm.ls fak_stav.ls fak_ura.ls fak_zag.ls \
            fakartgr.ls fakbroj.ls fakdbutl.ls fakgrart.ls faklock.ls fakpass.ls \
            fakpren.ls fakpren2.ls faktext.ls faktpbr.ls pselfakt.ls odobreno.ls \
            garancije.ls

all     : $(FAKFILES) $(FAKFILES2) 

## COMMON

pselfakt.ls:  pselfakt.lgc
    $(CC) $(CFLAGS) appl=pselfakt dotfle=$(COMD)
    @echo " ">pselfakt.ls

odobreno.ls:  odobreno.lgc
    $(CC) $(CFLAGS) appl=odobreno dotfle=$(COMD)
    @echo " ">odobreno.ls

fakpass.ls:  fakpass.lgc
    $(CC) $(CFLAGS) appl=fakpass dotfle=$(COMD)
    @echo " ">fakpass.ls

fakpren.ls:  fakpren.lgc
    $(CC) $(CFLAGS) appl=fakpren dotfle=$(COMD)
    @echo " ">fakpren.ls

garancije.ls:  garancije.lgc
    $(CC) $(CFLAGS) appl=garancije dotfle=$(COMD)
    @echo " ">garancije.ls

fakpren2.ls:  fakpren2.lgc
    $(CC) $(CFLAGS) appl=fakpren2 dotfle=$(COMD)
    @echo " ">fakpren2.ls

faktext.ls:  faktext.lgc
    $(CC) $(CFLAGS) appl=faktext dotfle=$(COMD)
    @echo " ">faktext.ls

faktpbr.ls:  faktpbr.lgc
    $(CC) $(CFLAGS) appl=faktpbr dotfle=$(COMD)
    @echo " ">faktpbr.ls

fak_stav.ls:  fak_stav.lgc
    $(CC) $(CFLAGS) appl=fak_stav dotfle=$(COMD)
    @echo " ">fak_stav.ls

fak_ura.ls:  fak_ura.lgc
    $(CC) $(CFLAGS) appl=fak_ura dotfle=$(COMD)
    @echo " ">fak_ura.ls

fak_zag.ls:  fak_zag.lgc
    $(CC) $(CFLAGS) appl=fak_zag dotfle=$(COMD)
    @echo " ">fak_zag.ls

fakartgr.ls:  fakartgr.lgc
    $(CC) $(CFLAGS) appl=fakartgr dotfle=$(COMD)
    @echo " ">fakartgr.ls

fakbroj.ls:  fakbroj.lgc
    $(CC) $(CFLAGS) appl=fakbroj dotfle=$(COMD)
    @echo " ">fakbroj.ls

fakdbutl.ls:  fakdbutl.lgc
    $(CC) $(CFLAGS) appl=fakdbutl dotfle=$(COMD)
    @echo " ">fakdbutl.ls

fakgrart.ls:  fakgrart.lgc
    $(CC) $(CFLAGS) appl=fakgrart dotfle=$(COMD)
    @echo " ">fakgrart.ls

faklock.ls:  faklock.lgc
    $(CC) $(CFLAGS) appl=faklock dotfle=$(COMD)
    @echo " ">faklock.ls


fak_flds.ls:  fak_flds.lgc
    $(CC) $(CFLAGS) appl=fak_flds dotfle=.\lib\fak_dd.fle 
    @echo " ">fak_flds.ls

fak_idx.ls:  fak_idx.lgc
    $(CC) $(CFLAGS) appl=fak_idx dotfle=$(COMD)
    @echo " ">fak_idx.ls

fak_lang.ls:  fak_lang.lgc
    $(CC) $(CFLAGS) appl=fak_lang dotfle=$(COMD)
    @echo " ">fak_lang.ls

fak_ngod.ls:  fak_ngod.lgc
    $(CC) $(CFLAGS) appl=fak_ngod dotfle=$(COMD)
    @echo " ">fak_ngod.ls

fak_ppar.ls:  fak_ppar.lgc
    $(CC) $(CFLAGS) appl=fak_ppar dotfle=$(COMD)
    @echo " ">fak_ppar.ls

fak_ppri.ls:  fak_ppri.lgc
    $(CC) $(CFLAGS) appl=fak_ppri dotfle=$(COMD)
    @echo " ">fak_ppri.ls

fak_pri.ls:  fak_pri.lgc
    $(CC) $(CFLAGS) appl=fak_pri dotfle=$(COMD)
    @echo " ">fak_pri.ls

fak_prm.ls:  fak_prm.lgc
    $(CC) $(CFLAGS) appl=fak_prm dotfle=$(COMD)
    @echo " ">fak_prm.ls


aruspro.ls:  aruspro.lgc
    $(CC) $(CFLAGS) appl=aruspro dotfle=$(COMD)
    @echo " ">aruspro.ls

arzm_blag.ls:  arzm_blag.lgc
    $(CC) $(CFLAGS) appl=arzm_blag dotfle=$(COMD)
    @echo " ">arzm_blag.ls

fak_a_dd.ls:  fak_a_dd.lgc
    $(CC) $(CFLAGS) appl=fak_a_dd dotfle=$(COMD)
    @echo " ">fak_a_dd.ls

fak_art.ls:  fak_art.lgc
    $(CC) $(CFLAGS) appl=fak_art dotfle=$(COMD)
    @echo " ">fak_art.ls

fak_dd.ls:  fak_dd.lgc
    $(CC) $(CFLAGS) appl=fak_dd dotfle=.\lib\fak_dd.fle 
    @echo " ">fak_dd.ls

fak_ddx.ls:  fak_ddx.lgc
    $(CC) $(CFLAGS) appl=fak_ddx dotfle=.\lib\fak_dd.fle 
    @echo " ">fak_ddx.ls

fak_default.ls:  fak_default.lgc
    $(CC) $(CFLAGS) appl=fak_default dotfle=$(COMD)
    @echo " ">fak_default.ls



#########################33
$(TD)fak_dd.fle:  fak_dd.lgc
    $(CC) $(CFLAGS) appl=fak_dd dotfle=$(TD)fak_dd.fle
    $(CC) -w genddx ddfile=fak_dd
    $(CC) $(CFLAGS) appl=fak_ddx dotfle=$(TD)fak_ddx.fle

$(TD)fak_flds.fle:  fak_flds.lgc
    $(CC) $(CFLAGS) appl=fak_flds dotfle=$(TD)fak_flds.fle

$(TD)fak.fle:  fak.lgc
    $(CC) $(CFLAGS) appl=fak dotfle=$(TD)fak.fle

$(TD)fak_art.fle:  fak_art.lgc
    $(CC) $(CFLAGS) appl=fak_art dotfle=$(TD)fak_art.fle

$(TD)fakpass.fle:  fakpass.lgc
    $(CC) $(CFLAGS) appl=fakpass dotfle=$(TD)fakpass.fle

$(TD)fak_pri.fle:  fak_pri.lgc
    $(CC) $(CFLAGS) appl=fak_pri dotfle=$(TD)fak_pri.fle

$(TD)fak_zag.fle:  fak_zag.lgc
    $(CC) $(CFLAGS) appl=fak_zag dotfle=$(TD)fak_zag.fle

$(TD)fak_zag_gt.fle:  fak_zag_gt.lgc
    $(CC) $(CFLAGS) appl=fak_zag_gt dotfle=$(TD)fak_zag_gt.fle

$(TD)fak_stav.fle:  fak_stav.lgc
    $(CC) $(CFLAGS) appl=fak_stav dotfle=$(TD)fak_stav.fle

$(TD)fakbroj.fle:  fakbroj.lgc
    $(CC) $(CFLAGS) appl=fakbroj dotfle=$(TD)fakbroj.fle

$(TD)faklock.fle:  faklock.lgc
    $(CC) $(CFLAGS) appl=faklock dotfle=$(TD)faklock.fle

$(TD)fakpren.fle:  fakpren.lgc
    $(CC) $(CFLAGS) appl=fakpren dotfle=$(TD)fakpren.fle

$(TD)fakpren2.fle:  fakpren2.lgc
    $(CC) $(CFLAGS) appl=fakpren2 dotfle=$(TD)fakpren2.fle

$(TD)fak_a_dd.fle:  fak_a_dd.lgc
    $(CC) $(CFLAGS) appl=fak_a_dd dotfle=$(TD)fak_a_dd.fle
    $(CC) $(CFLAGS) appl=fak_a_dd dotfle=$(AD)fak_a_dd.fle
    @echo " ">fak_a_dd.ts      

$(TD)fak_pris.fle:  fak_pris.lgc
    $(CC) $(CFLAGS) appl=fak_pris dotfle=$(TD)fak_pris.fle

$(TD)lfaktstd.fle:  lfaktstd.lgc
    $(CC) $(CFLAGS) appl=lfaktstd dotfle=$(TD)lfaktstd.fle

$(TD)faktext.fle:  faktext.lgc
    $(CC) $(CFLAGS) appl=faktext dotfle=$(TD)faktext.fle

$(TD)fak_star.fle:  fak_star.lgc
    $(CC) $(CFLAGS) appl=fak_star dotfle=$(TD)fak_star.fle

$(TD)lfak_art.fle:  lfak_art.lgc
    $(CC) $(CFLAGS) appl=lfak_art dotfle=$(TD)lfak_art.fle

$(TD)fak_prm.fle:  fak_prm.lgc
    $(CC) $(CFLAGS) appl=fak_prm dotfle=$(TD)fak_prm.fle

$(TD)fak_ppri.fle:  fak_ppri.lgc
    $(CC) $(CFLAGS) appl=fak_ppri dotfle=$(TD)fak_ppri.fle

$(TD)lfak_pre.fle:  lfak_pre.lgc
    $(CC) $(CFLAGS) appl=lfak_pre dotfle=$(TD)lfak_pre.fle

$(TD)fak_ura.fle:  fak_ura.lgc
    $(CC) $(CFLAGS) appl=fak_ura dotfle=$(TD)fak_ura.fle

$(TD)pselfakt.fle:  pselfakt.lgc
    $(CC) $(CFLAGS) appl=pselfakt dotfle=$(TD)pselfakt.fle

$(TD)lfaktda1.fle:  lfaktda1.lgc
    $(CC) $(CFLAGS) appl=lfaktda1 dotfle=$(TD)lfaktda1.fle

$(TD)lfaktna1.fle:  lfaktna1.lgc
    $(CC) $(CFLAGS) appl=lfaktna1 dotfle=$(TD)lfaktna1.fle

$(TD)lfaktnr1.fle:  lfaktnr1.lgc
    $(CC) $(CFLAGS) appl=lfaktnr1 dotfle=$(TD)lfaktnr1.fle

$(TD)lfaktdr1.fle:  lfaktdr1.lgc
    $(CC) $(CFLAGS) appl=lfaktdr1 dotfle=$(TD)lfaktdr1.fle

$(TD)lfaktdr1_ml.fle:  lfaktdr1_ml.lgc
    $(CC) $(CFLAGS) appl=lfaktdr1_ml dotfle=$(TD)lfaktdr1_ml.fle

$(TD)lfaktdr1_mls.fle:  lfaktdr1_mls.lgc
    $(CC) $(CFLAGS) appl=lfaktdr1_mls dotfle=$(TD)lfaktdr1_mls.fle

$(TD)karticagt.fle:  karticagt.lgc
    $(CC) $(CFLAGS) appl=karticagt dotfle=$(TD)karticagt.fle

$(TD)lfaktdb1.fle:  lfaktdb1.lgc
    $(CC) $(CFLAGS) appl=lfaktdb1 dotfle=$(TD)lfaktdb1.fle

$(TD)lfaktnb1.fle:  lfaktnb1.lgc
    $(CC) $(CFLAGS) appl=lfaktnb1 dotfle=$(TD)lfaktnb1.fle

$(TD)lfaktnb1_rab.fle:  lfaktnb1_rab.lgc
    $(CC) $(CFLAGS) appl=lfaktnb1_rab dotfle=$(TD)lfaktnb1_rab.fle

$(TD)lfaktnj6.fle:  lfaktnj6.lgc
    $(CC) $(CFLAGS) appl=lfaktnj6 dotfle=$(TD)lfaktnj6.fle

$(TD)lfaktnj7.fle:  lfaktnj7.lgc
    $(CC) $(CFLAGS) appl=lfaktnj7 dotfle=$(TD)lfaktnj7.fle

$(TD)lpok_arzm.fle:  lpok_arzm.lgc
    $(CC) $(CFLAGS) appl=lpok_arzm dotfle=$(TD)lpok_arzm.fle

$(TD)Afaktnb1.fle:  Afaktnb1.lgc
    $(CC) $(CFLAGS) appl=Afaktnb1 dotfle=$(TD)Afaktnb1.fle

$(TD)fak_ngod.fle:  fak_ngod.lgc
    $(CC) $(CFLAGS) appl=fak_ngod dotfle=$(TD)fak_ngod.fle

$(TD)lfaktext.fle:  lfaktext.lgc
    $(CC) $(CFLAGS) appl=lfaktext dotfle=$(TD)lfaktext.fle

$(TD)fak_ppar.fle:  fak_ppar.lgc
    $(CC) $(CFLAGS) appl=fak_ppar dotfle=$(TD)fak_ppar.fle

$(TD)fakdbutl.fle:  fakdbutl.lgc
    $(CC) $(CFLAGS) appl=fakdbutl dotfle=$(TD)fakdbutl.fle
    $(CC) $(CFLAGS) appl=fakdbutl dotfle=$(AD)fakdbutl.fle
    @echo " ">fakdbutl.ts      

$(TD)lfaktdc1.fle:  lfaktdc1.lgc
    $(CC) $(CFLAGS) appl=lfaktdc1 dotfle=$(TD)lfaktdc1.fle

$(TD)lfaktnc1.fle:  lfaktnc1.lgc
    $(CC) $(CFLAGS) appl=lfaktnc1 dotfle=$(TD)lfaktnc1.fle

$(TD)fak_lang.fle:  fak_lang.lgc
    $(CC) $(CFLAGS) appl=fak_lang dotfle=$(TD)fak_lang.fle

$(TD)fak_ctar.fle:  fak_ctar.lgc
    $(CC) $(CFLAGS) appl=fak_ctar dotfle=$(TD)fak_ctar.fle

$(TD)lfaktnj1.fle:  lfaktnj1.lgc
    $(CC) $(CFLAGS) appl=lfaktnj1 dotfle=$(TD)lfaktnj1.fle

$(TD)fakprirn.fle:  fakprirn.lgc
    $(CC) $(CFLAGS) appl=fakprirn dotfle=$(TD)fakprirn.fle

$(TD)lfaktnd1.fle:  lfaktnd1.lgc
    $(CC) $(CFLAGS) appl=lfaktnd1 dotfle=$(TD)lfaktnd1.fle

$(TD)lfaktdd1.fle:  lfaktdd1.lgc
    $(CC) $(CFLAGS) appl=lfaktdd1 dotfle=$(TD)lfaktdd1.fle

$(TD)lfaktnj2.fle:  lfaktnj2.lgc
    $(CC) $(CFLAGS) appl=lfaktnj2 dotfle=$(TD)lfaktnj2.fle

$(TD)lfaktnj5.fle:  lfaktnj5.lgc
    $(CC) $(CFLAGS) appl=lfaktnj5 dotfle=$(TD)lfaktnj5.fle

$(TD)lfaktnj8.fle:  lfaktnj8.lgc
    $(CC) $(CFLAGS) appl=lfaktnj8 dotfle=$(TD)lfaktnj8.fle

$(TD)logo_nb2.fle:  logo_nb2.lgc
    $(CC) $(CFLAGS) appl=logo_nb2 dotfle=$(TD)logo_nb2.fle

$(TD)lfaktnr6.fle:  lfaktnr6.lgc
    $(CC) $(CFLAGS) appl=lfaktnr6 dotfle=$(TD)lfaktnr6.fle

$(TD)lfakt_lsf.fle:  lfakt_lsf.lgc
    $(CC) $(CFLAGS) appl=lfakt_lsf dotfle=$(TD)lfakt_lsf.fle

$(TD)lfaktnhub.fle:  lfaktnhub.lgc
    $(CC) $(CFLAGS) appl=lfaktnhub dotfle=$(TD)lfaktnhub.fle

$(TD)lfaktng3.fle:  lfaktng3.lgc
    $(CC) $(CFLAGS) appl=lfaktng3 dotfle=$(TD)lfaktng3.fle

$(TD)lfaktdj2.fle:  lfaktdj2.lgc
    $(CC) $(CFLAGS) appl=lfaktdj2 dotfle=$(TD)lfaktdj2.fle

$(TD)lfakmpr1.fle:  lfakmpr1.lgc
    $(CC) $(CFLAGS) appl=lfakmpr1 dotfle=$(TD)lfakmpr1.fle

$(TD)fak_default.fle:  fak_default.lgc
    $(CC) $(CFLAGS) appl=fak_default dotfle=$(TD)fak_default.fle

$(TD)selfakpr.fle:  selfakpr.lgc
    $(CC) $(CFLAGS) appl=selfakpr dotfle=$(TD)selfakpr.fle

$(TD)fak_idx.fle:  fak_idx.lgc
    $(CC) $(CFLAGS) appl=fak_idx dotfle=$(TD)fak_idx.fle

$(TD)faktpbr.fle:  faktpbr.lgc
    $(CC) $(CFLAGS) appl=faktpbr dotfle=$(TD)faktpbr.fle

$(TD)l_fakbroj.fle:  l_fakbroj.lgc
    $(CC) $(CFLAGS) appl=l_fakbroj dotfle=$(TD)l_fakbroj.fle

$(TD)l_faktpbr.fle:  l_faktpbr.lgc
    $(CC) $(CFLAGS) appl=l_faktpbr dotfle=$(TD)l_faktpbr.fle

$(TD)lfaktrba.fle:  lfaktrba.lgc
    $(CC) $(CFLAGS) appl=lfaktrba dotfle=$(TD)lfaktrba.fle

$(TD)fakgrart.fle:  fakgrart.lgc
    $(CC) $(CFLAGS) appl=fakgrart dotfle=$(TD)fakgrart.fle

$(TD)lfakgrart.fle:  lfakgrart.lgc
    $(CC) $(CFLAGS) appl=lfakgrart dotfle=$(TD)lfakgrart.fle

$(TD)fakartgr.fle:  fakartgr.lgc
    $(CC) $(CFLAGS) appl=fakartgr dotfle=$(TD)fakartgr.fle

$(TD)lfakartgr.fle:  lfakartgr.lgc
    $(CC) $(CFLAGS) appl=lfakartgr dotfle=$(TD)lfakartgr.fle

$(TD)fakgr_rekap.fle:  fakgr_rekap.lgc
    $(CC) $(CFLAGS) appl=fakgr_rekap dotfle=$(TD)fakgr_rekap.fle

$(TD)fakgr_pprek.fle:  fakgr_pprek.lgc
    $(CC) $(CFLAGS) appl=fakgr_pprek dotfle=$(TD)fakgr_pprek.fle

$(TD)fakgt_pprek.fle:  fakgt_pprek.lgc
    $(CC) $(CFLAGS) appl=fakgt_pprek dotfle=$(TD)fakgt_pprek.fle

$(TD)fak_start.fle:  fak_start.lgc
    $(CC) $(CFLAGS) appl=fak_start dotfle=$(TD)fak_start.fle

$(TD)fak_mpupl.fle:  fak_mpupl.lgc
    $(CC) $(CFLAGS) appl=fak_mpupl dotfle=$(TD)fak_mpupl.fle

$(TD)fak_blag.fle:  fak_blag.lgc
    $(CC) $(CFLAGS) appl=fak_blag dotfle=$(TD)fak_blag.fle

$(TD)fak_nazfak.fle:  fak_nazfak.lgc
    $(CC) $(CFLAGS) appl=fak_nazfak dotfle=$(TD)fak_nazfak.fle

$(TD)aruspro.fle:  aruspro.lgc
    $(CC) $(CFLAGS) appl=aruspro dotfle=$(TD)aruspro.fle

$(TD)fblag.fle:  fblag.lgc
    $(CC) $(CFLAGS) appl=fblag dotfle=$(TD)fblag.fle

$(TD)grp_arzm.fle:  grp_arzm.lgc
    $(CC) $(CFLAGS) appl=grp_arzm dotfle=$(TD)grp_arzm.fle

$(TD)ref_arzm.fle:  ref_arzm.lgc
    $(CC) $(CFLAGS) appl=ref_arzm dotfle=$(TD)ref_arzm.fle

$(TD)lfaktng1.fle:  lfaktng1.lgc
    $(CC) $(CFLAGS) appl=lfaktng1 dotfle=$(TD)lfaktng1.fle

$(TD)lnazfak_err.fle:  lnazfak_err.lgc
    $(CC) $(CFLAGS) appl=lnazfak_err dotfle=$(TD)lnazfak_err.fle

$(TD)lnazfak_ppar.fle:  lnazfak_ppar.lgc
    $(CC) $(CFLAGS) appl=lnazfak_ppar dotfle=$(TD)lnazfak_ppar.fle

$(TD)lista_arzm.fle:  lista_arzm.lgc
    $(CC) $(CFLAGS) appl=lista_arzm dotfle=$(TD)lista_arzm.fle

$(TD)lgrupe_arzm.fle:  lgrupe_arzm.lgc
    $(CC) $(CFLAGS) appl=lgrupe_arzm dotfle=$(TD)lgrupe_arzm.fle

$(TD)lref_arzm.fle:  lref_arzm.lgc
    $(CC) $(CFLAGS) appl=lref_arzm dotfle=$(TD)lref_arzm.fle

$(TD)lkup_usl.fle:  lkup_usl.lgc
    $(CC) $(CFLAGS) appl=lkup_usl dotfle=$(TD)lkup_usl.fle

$(TD)lkup_arzm.fle:  lkup_arzm.lgc
    $(CC) $(CFLAGS) appl=lkup_arzm dotfle=$(TD)lkup_arzm.fle

$(TD)arzm_blag.fle:  arzm_blag.lgc
    $(CC) $(CFLAGS) appl=arzm_blag dotfle=$(TD)arzm_blag.fle

$(TD)arzm_allup.fle:  arzm_allup.lgc
    $(CC) $(CFLAGS) appl=arzm_allup dotfle=$(TD)arzm_allup.fle

$(TD)arzm_avans.fle:  arzm_avans.lgc
    $(CC) $(CFLAGS) appl=arzm_avans dotfle=$(TD)arzm_avans.fle

$(TD)lfaktng2.fle:  lfaktng2.lgc
    $(CC) $(CFLAGS) appl=lfaktng2 dotfle=$(TD)lfaktng2.fle

$(TD)lfaktng5.fle:  lfaktng5.lgc
    $(CC) $(CFLAGS) appl=lfaktng5 dotfle=$(TD)lfaktng5.fle

$(TD)uplatnica.fle:  uplatnica.lgc
    $(CC) $(CFLAGS) appl=uplatnica dotfle=$(TD)uplatnica.fle

$(TD)lfaktnj3.fle:  lfaktnj3.lgc
    $(CC) $(CFLAGS) appl=lfaktnj3 dotfle=$(TD)lfaktnj3.fle

$(TD)lfaktnj4.fle:  lfaktnj4.lgc
    $(CC) $(CFLAGS) appl=lfaktnj4 dotfle=$(TD)lfaktnj4.fle

$(TD)lfakt_nazfak.fle:  lfakt_nazfak.lgc
    $(CC) $(CFLAGS) appl=lfakt_nazfak dotfle=$(TD)lfakt_nazfak.fle

$(TD)lfak_strval.fle:  lfak_strval.lgc
    $(CC) $(CFLAGS) appl=lfak_strval dotfle=$(TD)lfak_strval.fle

$(TD)lclose_kalk.fle:  lclose_kalk.lgc
    $(CC) $(CFLAGS) appl=lclose_kalk dotfle=$(TD)lclose_kalk.fle

$(TD)arzm_file.fle:  arzm_file.lgc
    $(CC) $(CFLAGS) appl=arzm_file dotfle=$(TD)arzm_file.fle

$(TD)usluge_file.fle:  usluge_file.lgc
    $(CC) $(CFLAGS) appl=usluge_file dotfle=$(TD)usluge_file.fle

$(TD)larzm_sk.fle:  larzm_sk.lgc
    $(CC) $(CFLAGS) appl=larzm_sk dotfle=$(TD)larzm_sk.fle

$(TD)fak_rabati.fle:  fak_rabati.lgc
    $(CC) $(CFLAGS) appl=fak_rabati dotfle=$(TD)fak_rabati.fle

$(TD)fakgr_acrek.fle:  fakgr_acrek.lgc
    $(CC) $(CFLAGS) appl=fakgr_acrek dotfle=$(TD)fakgr_acrek.fle

$(TD)lkup_ref.fle:  lkup_ref.lgc
    $(CC) $(CFLAGS) appl=lkup_ref dotfle=$(TD)lkup_ref.fle

$(TD)fakparjez.fle:  fakparjez.lgc
    $(CC) $(CFLAGS) appl=fakparjez dotfle=$(TD)fakparjez.fle

$(TD)garancije.fle:  garancije.lgc
    $(CC) $(CFLAGS) appl=garancije dotfle=$(TD)garancije.fle

$(TD)lfaktnh1.fle:  lfaktnh1.lgc
    $(CC) $(CFLAGS) appl=lfaktnh1 dotfle=$(TD)lfaktnh1.fle

$(TD)odobreno.fle:  odobreno.lgc
    $(CC) $(CFLAGS) appl=odobreno dotfle=$(TD)odobreno.fle

$(TD)lfakt_isp.fle:  lfakt_isp.lgc
    $(CC) $(CFLAGS) appl=lfakt_isp dotfle=$(TD)lfakt_isp.fle

$(TD)lponuda.fle:  lponuda.lgc
    $(CC) $(CFLAGS) appl=lponuda dotfle=$(TD)lponuda.fle

$(TD)txt_pdv0.fle:  txt_pdv0.lgc
    $(CC) $(CFLAGS) appl=txt_pdv0 dotfle=$(TD)txt_pdv0.fle

$(TD)logo_nb1.fle:  logo_nb1.lgc
    $(CC) $(CFLAGS) appl=logo_nb1 dotfle=$(TD)logo_nb1.fle

$(TD)fak_str_sld.fle:  fak_str_sld.lgc
    $(CC) $(CFLAGS) appl=fak_str_sld dotfle=$(TD)fak_str_sld.fle

$(TD)loadinka.fle:  loadinka.lgc
    $(CC) $(CFLAGS) appl=loadinka dotfle=$(TD)loadinka.fle

$(TD)loadinkast.fle:  loadinkast.lgc
    $(CC) $(CFLAGS) appl=loadinkast dotfle=$(TD)loadinkast.fle

$(TD)faktip.fle:  faktip.lgc
    $(CC) $(CFLAGS) appl=faktip dotfle=$(TD)faktip.fle

$(TD)storno1za1.fle:  storno1za1.lgc
    $(CC) $(CFLAGS) appl=storno1za1 dotfle=$(TD)storno1za1.fle

$(TD)lfaktne1.fle:  lfaktne1.lgc
    $(CC) $(CFLAGS) appl=lfaktne1 dotfle=$(TD)lfaktne1.fle

$(TD)lfaktne2.fle:  lfaktne2.lgc
    $(CC) $(CFLAGS) appl=lfaktne2 dotfle=$(TD)lfaktne2.fle

$(TD)lfaktns1.fle:  lfaktns1.lgc
    $(CC) $(CFLAGS) appl=lfaktns1 dotfle=$(TD)lfaktns1.fle

$(TD)fak_rev.fle:  fak_rev.lgc
    $(CC) $(CFLAGS) appl=fak_rev dotfle=$(TD)fak_rev.fle
    COPY fak_rev.lgc $(YTD)fak_rev.lgc

$(TD)fak_2012.fle:  fak_2012.lgc
    $(CC) $(CFLAGS) appl=fak_2012 dotfle=$(TD)fak_2012.fle

$(TD)lfaktna2.fle:  lfaktna2.lgc
    $(CC) $(CFLAGS) appl=lfaktna2 dotfle=$(TD)lfaktna2.fle

$(TD)lfaktda2.fle:  lfaktda2.lgc
    $(CC) $(CFLAGS) appl=lfaktda2 dotfle=$(TD)lfaktda2.fle

$(TD)lfaktnj9.fle:  lfaktnj9.lgc
    $(CC) $(CFLAGS) appl=lfaktnj9 dotfle=$(TD)lfaktnj9.fle

$(TD)lfaktdj9.fle:  lfaktdj9.lgc
    $(CC) $(CFLAGS) appl=lfaktdj9 dotfle=$(TD)lfaktdj9.fle

$(TD)lfaktnm1.fle:  lfaktnm1.lgc
    $(CC) $(CFLAGS) appl=lfaktnm1 dotfle=$(TD)lfaktnm1.fle

$(TD)lfaktno1.fle:  lfaktno1.lgc
    $(CC) $(CFLAGS) appl=lfaktno1 dotfle=$(TD)lfaktno1.fle

$(TD)lfakthub.fle:  lfakthub.lgc
    $(CC) $(CFLAGS) appl=lfakthub dotfle=$(TD)lfakthub.fle

$(TD)stav_2012.fle:  stav_2012.lgc
    $(CC) $(CFLAGS) appl=stav_2012 dotfle=$(TD)stav_2012.fle

$(TD)fakturisti.fle:  fakturisti.lgc
    $(CC) $(CFLAGS) appl=fakturisti dotfle=$(TD)fakturisti.fle

$(TD)lfaktnc2.fle:  lfaktnc2.lgc
    $(CC) $(CFLAGS) appl=lfaktnc2 dotfle=$(TD)lfaktnc2.fle

$(TD)lfaktnc3.fle:  lfaktnc3.lgc
    $(CC) $(CFLAGS) appl=lfaktnc3 dotfle=$(TD)lfaktnc3.fle

$(TD)lfaktnc4.fle:  lfaktnc4.lgc
    $(CC) $(CFLAGS) appl=lfaktnc4 dotfle=$(TD)lfaktnc4.fle

$(TD)fak_hpagot.fle:  fak_hpagot.lgc
    $(CC) $(CFLAGS) appl=fak_hpagot dotfle=$(TD)fak_hpagot.fle

$(TD)hpa_zag.fle:  hpa_zag.lgc
    $(CC) $(CFLAGS) appl=hpa_zag dotfle=$(TD)hpa_zag.fle

$(TD)hpa_stavke.fle:  hpa_stavke.lgc
    $(CC) $(CFLAGS) appl=hpa_stavke dotfle=$(TD)hpa_stavke.fle

$(TD)lfaktdt1.fle:  lfaktdt1.lgc
    $(CC) $(CFLAGS) appl=lfaktdt1 dotfle=$(TD)lfaktdt1.fle

$(TD)lfaktnt1.fle:  lfaktnt1.lgc
    $(CC) $(CFLAGS) appl=lfaktnt1 dotfle=$(TD)lfaktnt1.fle

$(TD)fak_pokrica.fle:  fak_pokrica.lgc
    $(CC) $(CFLAGS) appl=fak_pokrica dotfle=$(TD)fak_pokrica.fle

$(TD)lfaktne3.fle:  lfaktne3.lgc
    $(CC) $(CFLAGS) appl=lfaktne3 dotfle=$(TD)lfaktne3.fle

$(TD)lponuda2.fle:  lponuda2.lgc
    $(CC) $(CFLAGS) appl=lponuda2 dotfle=$(TD)lponuda2.fle

$(TD)lponuda3.fle:  lponuda3.lgc
    $(CC) $(CFLAGS) appl=lponuda3 dotfle=$(TD)lponuda3.fle

$(TD)storno_dio.fle:  storno_dio.lgc
    $(CC) $(CFLAGS) appl=storno_dio dotfle=$(TD)storno_dio.fle

$(TD)fakture_ext.fle:  fakture_ext.lgc
    $(CC) $(CFLAGS) appl=fakture_ext dotfle=$(TD)fakture_ext.fle

$(TD)fak_ext_st.fle:  fak_ext_st.lgc
    $(CC) $(CFLAGS) appl=fak_ext_st dotfle=$(TD)fak_ext_st.fle

$(TD)load_milk.fle:  load_milk.lgc
    $(CC) $(CFLAGS) appl=load_milk dotfle=$(TD)load_milk.fle

$(TD)lfaktnh2.fle:  lfaktnh2.lgc
    $(CC) $(CFLAGS) appl=lfaktnh2 dotfle=$(TD)lfaktnh2.fle

$(TD)lfakt_eaap.fle:  lfakt_eaap.lgc
    $(CC) $(CFLAGS) appl=lfakt_eaap dotfle=$(TD)lfakt_eaap.fle

